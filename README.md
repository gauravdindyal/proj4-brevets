Author: Gaurav Dindyal
email: gauravd@uoregon.edu
ACP time Calculator:
This program calculates control times of bike race.
The python program takes the selected race distance depending on the km point given.
This calculator calculates ACP controle times, using the algorithm described here
 (https://rusa.org/pages/acp-brevet-control-times-calculator) and implemented here
 (https://rusa.org/octime_acp.html), using AJAX and Flask. 
It can be run by building a docker container, running it in the background, and then visiting the specified port.